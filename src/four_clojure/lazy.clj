(ns four-clojure.lazy)


(defn positive-numbers
  ([] (positive-numbers 1))
  ([n] (lazy-seq (cons n (positive-numbers (inc n))))))

(take 5 (positive-numbers))

(defn fib [a b]
  (lazy-seq (cons a (fib b (+ b a)))))

(take 5 (fib 1 1))

(first (range))
(rest (range))
