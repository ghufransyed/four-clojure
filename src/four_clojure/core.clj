(ns four-clojure.core
  (require [clojure.set])
  (require [clojure.string])
  (:import (java.util.Collections)
           (String)))

; palindrome detector
; Write a function which returns true if the given sequence is a palindrome.


(defn palindrome? [coll]
  (let [first-item (first coll)
        last-item (last coll)
        middle-bit (rest (butlast coll))]
    (if (empty? coll)
      true
      (and (= first-item last-item)
           (palindrome? middle-bit)))))

; better version:

(defn palindrome? [x] (= (seq x) (reverse x)))


(false? (palindrome? '(1 2 3 4 5)))

(true? (palindrome? "racecar"))

(true? (palindrome? [:foo :bar :foo]))

(true? (palindrome? '(1 1 3 3 1 1)))

(false? (palindrome? '(:a :b :c)))

; Maximum value
;Write a function which takes a variable number of parameters and returns the maximum value

(defn my-max [& coll]
  (reduce (fn [x y] (if (> x y)
                      x
                      y))
          coll))

(my-max 1 8 3 4)                                             ;=> 8
(my-max 30 20)                                               ;=> 30
(my-max 45 67 11)                                            ;=> 67

; Get the Caps
; Write a function which takes a string and returns 
; a new string containing only the capital letters.

(defn getcaps [str-arg]
  (apply str (filter #(Character/isUpperCase %) str-arg)))

(def getcaps #(apply str (re-seq #"[A-Z]" %)))

(= (getcaps "HeLlO, WoRlD!") "HLOWRD")
(getcaps "HeLlO, WoRlD!")
(empty? (getcaps "nothing"))
(= (getcaps "$#A(*&987Zf") "AZ")


; Duplicate a Sequence
; Write a function which duplicates each element of a sequence.

(defn dup-seq [coll]
  (mapcat #(list % %) coll))

(defn dup-seq [coll]
  (#(interleave % %) coll))

(= (dup-seq [1 2 3]) '(1 1 2 2 3 3))
(dup-seq [1 2 3])                                           ;'(1 1 2 2 3 3)
(#(interleave % %) [1 2 3])


(= (dup-seq [:a :a :b :b]) '(:a :a :a :a :b :b :b :b))

(= (dup-seq [[1 2] [3 4]]) '([1 2] [1 2] [3 4] [3 4]))

(= (dup-seq [[1 2] [3 4]]) '([1 2] [1 2] [3 4] [3 4]))

; Intro to some
; The some function takes a predicate function and a collection.
; It returns the first logical true value of (predicate x) where x is an item in the collection.

(some #{2 7 6} [5 6 7 8])

; Implement range
; Write a function that creates a list of all integers in a given range

(defn my-range [start end]
  (take (- end start) (iterate inc start))) 

(= (my-range 1 4) '(1 2 3))
(my-range 1 4)
(= (my-range -2 2) '(-2 -1 0 1))
(= (my-range 5 8) '(5 6 7))

; Factorial fun
; Write a function which calculates factorials.

(defn factorial [num]
  (apply * (range 1 (inc num))))


(factorial 1)                                               ;=> 1 
(factorial 3)                                               ;=> 6
(factorial 5)                                               ;=> 120
(factorial 8)                                               ;=> 40320

; Compress a sequence
; Write a function which removes consecutive duplicates from a sequence.

(defn remove-dups [coll]
  (conj (map last (filter (fn [[x y]] (not= x y))
                          (partition 2 1 coll)))
        (first coll)))

;; better solution
(defn remove-dups [coll]
  (map first (partition-by identity coll)))

(partition-by identity "Leeeeerrrroyyy")

(remove-dups "Leeeeeeeerrrroy")
(remove-dups [1 1 2 3 3 2 2 3])

(= (apply str (remove-dups "Leeeeeerrroyyy")) "Leroy")
(= (remove-dups [1 1 2 3 3 2 2 3]) '(1 2 3 2 3))
(= (remove-dups [[1 2] [1 2] [3 4] [1 2]]) '([1 2] [3 4] [1 2]))

; Interleave two seqs
; write a function which takes two sequences and returns the first item
; from each, then the second item from each, then the third, etc.


(defn my-interleave [coll1 coll2]
  (if (some empty? [coll1 coll2])
    '()
    (concat (list (first coll1) (first coll2))
            (my-interleave (rest coll1) (rest coll2)))))

(defn my-interleave [coll1 coll2]
  (mapcat vector coll1 coll2))



(apply concat (map vector [1 2 3] [:a :b :c]))              ; is equivalent to
(mapcat vector [1 2 3] [:a :b :c])

(= (my-interleave [1 2 3] [:a :b :c]) '(1 :a 2 :b 3 :c))
(= (my-interleave [1 2] [3 4 5 6]) '(1 3 2 4))
(= (my-interleave [1 2 3 4] [5]) [1 5])
(= (my-interleave [30 20] [25 15]) [30 25 20 15])

; Flatten a sequence
; Write a function that flattens a sequence

#_(defn my-flatten [coll]
  (if (sequential? coll)
    (mapcat (fn [x] (if-not (sequential? x)
                   (list x)
                   (my-flatten x))) 
            coll)
    coll))

(defn my-flatten [coll]
  (mapcat (fn [x] (if-not (sequential? x)
                    (list x)
                    (my-flatten x)))
          coll))


;; other solution (very similar)
(defn my-flatten [x]
  (if (coll? x)
    (mapcat my-flatten x)
    [x]))

(my-flatten '((1 2) 3 [4 [5 6]])) 
(my-flatten 3)
(my-flatten '(3))
(my-flatten '(3 4))
(my-flatten '((:a)))

(def test-seq '(1 2 '(3) 4 5))
(map identity test-seq)
(apply concat (map list [1 3 5] [2 4 6]))
(mapcat list [1 3 5] [2 4 6])


(concat (list 3) [1 2 3])

(= (my-flatten '((1 2) 3 [4 [5 6]])) '(1 2 3 4 5 6))
(= (my-flatten ["a" ["b"] "c"]) '("a" "b" "c"))
(= (my-flatten '((((:a))))) '(:a))

; replicate a sequence
; Write a function which replicates each element 
; of a sequence a variable number of times.

(defn my-replicate [coll n]
  (mapcat #(repeat n %) coll))
(my-replicate [1 2 3] 2) '(1 1 2 2 3 3)
(= (my-replicate [1 2 3] 2) '(1 1 2 2 3 3))
(= (my-replicate [:a :b] 4) '(:a :a :a :a :b :b :b :b))
(= (my-replicate [4 5 6] 1) '(4 5 6))
(= (my-replicate [[1 2] [3 4]] 2) '([1 2] [1 2] [3 4] [3 4]))
(= (my-replicate [44 33] 2) [44 44 33 33])

; interpose a seq
; Write a function which separates the items of a sequence by an arbitrary value.

#_(defn my-interpose [sep coll]
  (rest (mapcat vector (repeat sep) coll)))

; alternative:

(defn my-interpose [sep coll]
  (drop-last (interleave coll (repeat sep) )))

#_(def my-interpose #(drop-last (interleave %2 (repeat %1))))

(my-interpose 0 [1 2 3]) [1 0 2 0 3]
(= (my-interpose 0 [1 2 3]) [1 0 2 0 3])
(= (apply str (my-interpose ", " ["one" "two" "three"])) "one, two, three")
(= (my-interpose :z [:a :b :c :d]) [:a :z :b :z :c :z :d])

; Pack a Sequence
; Write a function which packs consecutive duplicates into sub-lists.


(defn pack [coll]
  (partition-by identity coll))


(pack [1 1 2 1 1 1 3 3])                                    ;=> '((1 1) (2) (1 1 1) (3 3))
(= (pack [1 1 2 1 1 1 3 3]) '((1 1) (2) (1 1 1) (3 3)))
(= (pack [:a :a :b :b :c]) '((:a :a) (:b :b) (:c)))
(= (pack [[1 2] [1 2] [3 4]]) '(([1 2] [1 2]) ([3 4])))

; Map defaults
; Write a function which takes a default value 
; and a sequence of keys and constructs a map.

#_(defn map-default [default key-seq]
  (apply hash-map (mapcat vector key-seq (repeat default))))

; better solution
(defn map-default [default key-seq]
      (zipmap key-seq (repeat default)))
  
(repeat 10 :foo)
(map-default 0 [:a :b :c]) 
(= (map-default 0 [:a :b :c]) {:a 0 :b 0 :c 0})
(= (map-default "x" [1 2 3]) {1 "x" 2 "x" 3 "x"})
(= (map-default [:a :b] [:foo :bar]) {:foo [:a :b] :bar [:a :b]})

; Drop every Nth item
; Write a function which drops every Nth item from a sequence.

#_(defn drop-every-n [coll n]
  (mapcat (fn [x] (if (<  (count x) n)
                    x
                    (drop-last x))) 
          (partition-all n coll)))

(defn drop-every-n [coll n]
  (apply concat (partition-all (dec n) n coll)))

(def coll (range 20) )
(def n 3)
(dec n)
(partition-all (dec n)  coll)
(partition-all (dec n) n coll)


(partition-all 3 (range 20))
(partition-all 3 2 (range 20))


(drop-every-n [1 2 3 4 5 6 7 8] 3) 

(= (drop-every-n [1 2 3 4 5 6 7 8] 3) [1 2 4 5 7 8])
(= (drop-every-n [:a :b :c :d :e :f] 2) [:a :c :e])
(= (drop-every-n [1 2 3 4 5 6] 4) [1 2 3 5 6])

; Split a sequence
; Write a function which will split a sequence into two parts.

(defn my-split-at [n coll]
  (vector (take n coll)
          (drop n coll)))

; alternative solution
(def my-split-at (juxt take drop) )

(list (take 2 [1 2 3 4]) (drop 2 [1 2 3 4]))

(def my-map {:a 1 :b 2 :c 3 :d 4})
my-map
(:b my-map)
(:d my-map)
[(:b my-map) (:d my-map)]
[(:a my-map) (:d my-map)]

((juxt :a :c)   my-map)


(= (my-split-at 3 [1 2 3 4 5 6]) [[1 2 3] [4 5 6]])
(= (my-split-at 1 [:a :b :c :d]) [[:a] [:b :c :d]])
(= (my-split-at 2 [[1 2] [3 4] [5 6]]) [[[1 2] [3 4]] [[5 6]]])

(defn first-char-and-length [a-string]
  (vector (first a-string)
          (count a-string)))

(first-char-and-length "Clojure is awesome")

((juxt first count) "Clojure is awesome")

(let [a-str "Clojure is awesome"]
  (vector (first a-str)
          (count a-str)))

(defn first-char-and-length [a-string]
  ((juxt first count) a-string))

; flipping out
; Write a higher-order function which flips the order 
; of the arguments of an input function.

(defn flip-args [func]
  (fn [x y] (func y x)))

(= 3 ((flip-args nth) 2 [1 2 3 4 5]))
(= true ((flip-args >) 7 8))
(= 4 ((flip-args quot) 2 8))
(= [1 2 3] ((flip-args take) [1 2 3 4 5] 3))

; A half-truth
; Write a function which takes a variable number of booleans. 
; Your function should return true if some of the parameters are true, 
; but not all of the parameters are true. 
; Otherwise your function should return false.

(defn half-truth? [& args]
  (cond 
    (every? #(or %) args) false
    (not-any? #(or %) args) false
    :else true))

; alternative solution!!
not=

(def half-truth? not=)

(not= false false false)
(not= true true true)
(not= true false true)



(every? #(or %) [false true])

(or true)
(or false)


(= false (half-truth? false false))
(= true (half-truth? true false))
(= false (half-truth? true))
(= true (half-truth? false true false))
(= false (half-truth? true true true))
(= true (half-truth? true true true false))

; map construction
;
; Write a function which takes a vector of keys and
; a vector of values and constructs a map from them.
; test not run

(defn construct-map [key-coll val-coll]
  (apply hash-map (mapcat vector key-coll val-coll)))

; alternative
(defn construct-map [key-coll val-coll]
  (apply hash-map (interleave key-coll val-coll)))

(construct-map [:a :b :c] [1 2 3])
(= (construct-map [:a :b :c] [1 2 3]) {:a 1, :b 2, :c 3})
(= (construct-map [1 2 3 4] ["one" "two" "three"]) {1 "one", 2 "two", 3 "three"})
(= (construct-map [:foo :bar] ["foo" "bar" "baz"]) {:foo "foo", :bar "bar"})

; greatest common divisor
; Given two integers, write a function which 
; returns the greatest common divisor.

(defn gcd [x y]
  (let [small (min x y)
        big   (max x y)]
    (->> (range 1 (inc small))
         (filter #(and (= 0 (rem big %))
                       (= 0 (rem small %))))
         (last))))

; better solution
(defn gcd [x y]
  (let [small (min x y)
        big   (max x y)]
    (->> (inc small)
         (range 1 )
         (filter #(= 0 (rem big %) (rem small %)))
         (last))))


(= (gcd 2 4) 2)
(= (gcd 10 5) 5)
(= (gcd 5 7) 1)
(= (gcd 1023 858) 33)

; set intersection
; Write a function which returns the intersection of two sets. 
; The intersection is the sub-set of items that each set has in common.

(defn my-intersection [s1 s2]
  (->> (concat s1 s2)
       (filter #(and (contains? s1 %)
                     (contains? s2 %)))
       (set)))

(def my-intersection (comp set filter))

(def s1 #{0 1 2 3})
(def s2 #{2 3 4 5})
(filter s1 s2)
(s1 4)
(filter s1  (range 10))

(my-intersection #{0 1 2 3} #{2 3 4 5})
(concat #{0 1 2 3} #{2 3 4 5})
(distinct (concat #{0 1 2 3} #{2 3 4 5}))
(clojure.set/union #{0 1 2 3} #{2 3 4 5})

(= (my-intersection #{0 1 2 3} #{2 3 4 5}) #{2 3})
(= (my-intersection #{0 1 2} #{3 4 5}) #{})
(= (my-intersection #{:a :b :c :d} #{:c :e :a :f :d}) #{:a :c :d})

(def banned #{"Steve" "Michael"})
(def guest-list ["Brian" "Josh" "Steve"])

(remove banned guest-list)

; comparisons
; Write a function that takes three arguments, a less than operator for the data and two items to compare. The function should return a keyword describing the relationship between the two items. The keywords for the relationship between x and y are as follows:
; x = y → :eq
; x > y → :gt
; x < y → :lt

(defn foo [op x y]
  (cond
    (op x y)    :lt
    (op y x)    :gt
    :else       :eq))


(= :gt (foo < 5 1))
(= :eq (foo (fn [x y] (< (count x) (count y))) "pear" "plum"))
(= :lt (foo (fn [x y] (< (mod x 5) (mod y 5))) 21 3))
(= :gt (foo > 0 2))

; re-implement iterate
; Given a side-effect free function f and an initial value x write a function which returns an infinite lazy sequence of x, (f x), (f (f x)), (f (f (f x))), etc.

(defn my-iterate [f x]
  (lazy-cat [x] (my-iterate f (f x))))

(take 5 (my-iterate #(* 2 %) 1))

(= (take 5 (my-iterate #(* 2 %) 1)) [1 2 4 8 16])
(= (take 100 (my-iterate inc 0)) (take 100 (range)))
(= (take 9 (my-iterate #(inc (mod % 3)) 1)) (take 9 (cycle [1 2 3])))
(take 5 (my-iterate #(* 2 %) 1))
(take 100 (my-iterate inc 0))
(take 9 (my-iterate #(inc (mod % 3)) 1))


; simple closures
; write a function that given a positive integer n, returns a function (f x) which computes x^n. 

(defn exp-maker [n]
  (fn [x] (int (Math/pow x n))))


(= 256 ((exp-maker 2) 16) ((exp-maker 8) 2))

(= [1 8 27 64] (map (exp-maker 3) [1 2 3 4]))

(= [1 2 4 8 16] (map #((exp-maker %) 2) [0 1 2 3 4]))

; product digits
; Write a function which multiplies two numbers and returns the result as a sequence of its digits.

(defn product-digits [x y]
  (->> (* x y)
       (str)
       (seq)
       (map #(Character/getNumericValue %))
       (apply vector)))

(seq "123") 
(product-digits 12 13)


(= (product-digits 1 1) [1])

(= (product-digits 99 9) [8 9 1])

(= (product-digits 999 99) [9 8 9 0 1])


; cartesian product
; Write a function which calculates the Cartesian product of two sets.

(defn cart-prod [s1 s2]
  (for [p s1
        q s2]
    [p q]))

(cart-prod s1 s2)

(def s1 #{1 2 3})
(def s2 #{4 5})

(apply vector s1)
(apply vector s2)
(merge s2 s1)



(= (cart-prod #{"ace" "king" "queen"} #{"♠" "♥" "♦" "♣"})
   #{["ace"   "♠"] ["ace"   "♥"] ["ace"   "♦"] ["ace"   "♣"]
     ["king"  "♠"] ["king"  "♥"] ["king"  "♦"] ["king"  "♣"]
     ["queen" "♠"] ["queen" "♥"] ["queen" "♦"] ["queen" "♣"]})


(= (cart-prod #{1 2 3} #{4 5})
   #{[1 4] [2 4] [3 4] [1 5] [2 5] [3 5]})

(= 300 (count (cart-prod (into #{} (range 10))
                  (into #{} (range 30)))))


; group a sequence
; Given a function f and a sequence s, write a function which returns a map. The keys should be the values of f applied to each item in s. The value at each key should be a vector of corresponding items in the order they appear in s.

#_(defn my-group-by [f s]
  (apply hash-map (interleave (distinct (map f s))
                              (map #(apply vector %)
                                   (partition-by f s)))))
;; not working, because partition-by partitions into groups not by
;; absolute value, but when value changes

#_(defn my-group-by [f coll]
  (reduce (fn [result-map x]
            (let [a-key (f x)]
              (assoc result-map a-key (conj (get result-map a-key []) x)))) {} coll))

(defn my-group-by [f coll]
  (reduce (fn [result-map x]
            (let [a-key (f x)]
              (assoc result-map a-key (conj (get result-map a-key [])
                                            x))))
          {}
          coll))

;; learning points
;; to 'make' a map by adding elements, need to reduce, starting with an empty map
;; the anonymous function passed to reduce needs to take two args, the map-so-far 
;; and the current element
;; to 'update' the map, need to assoc onto it
;; to add each new value, use conj
;; if the key is not in the result-map so far, use the default value in get
;; to provide empty vector

;; alternative version

(defn my-group-by [f s]
  (apply merge-with concat (map #(hash-map (f %1) [%1]) s)))

;; this creates lists of 'similar' elements of s, not vectors
;; using 'into' instead of concat preserves the 'shape' of the value


(defn my-group-by [f s]
  (apply merge-with into (map #(hash-map (f %1) [%1]) s)))


(my-group-by #(> % 5) [1 3 6 8])

(my-group-by #(apply / %) [[1 2] [2 4] [4 6] [3 6]])

{1/2 [[1 2] [2 4] [3 6]], 2/3 [[4 6]]}

(= (my-group-by #(apply / %) [[1 2] [2 4] [4 6] [3 6]])
   {1/2 [[1 2] [2 4] [3 6]], 2/3 [[4 6]]})



(def s [1 3 6 8])
(def f #(> % 5))
(def g #(apply / %))
(map #(hash-map (f %1) [%1]) s)
(apply merge-with  into (map #(hash-map (f %1) [%1]) s))
(concat [1] [2])



(def t [[1 2] [2 4] [4 6] [3 6]])
s
f
(interleave (map f s) s)
(apply hash-map (interleave (distinct (map f s))
                     (repeat [])))
(apply hash-map (interleave (distinct (map f s))
                            (map #(apply vector %) (partition-by f s))))

(partition-by g t)
(partition-by identity t)
(map #(apply vector %) (partition-by f s))


(partition-by f s)
(map #(apply vector %) (partition-by f s))
(def g #(apply / %))
(partition-by g [[1 2] [2 4] [4 6] [3 6]])

(= {:a 1 :b 2} {:b 2 :a 1} )


;; practice - start again from scratch
(defn my-group-by [f s]
  (reduce (fn [rmap x]
            (assoc rmap (f x) (conj (get rmap (f x) [])
                                    x))) 
          {} 
          s))

;; and other version

(defn my-group-by [f s]
  (apply merge-with into (map (fn [x] (hash-map (f x) [x])) s)))

(my-group-by #(> % 5) [1 3 6 8])






(= (my-group-by #(> % 5) [1 3 6 8]) {false [1 3], true [6 8]})
(= (my-group-by #(apply / %) [[1 2] [2 4] [4 6] [3 6]])
   {1/2 [[1 2] [2 4] [3 6]], 2/3 [[4 6]]})
(= (my-group-by count [[1] [1 2] [3] [1 2 3] [2 3]])
   {1 [[1] [3]], 2 [[1 2] [2 3]], 3 [[1 2 3]]})

; Symmetric difference
; Write a function which returns the symmetric difference of two sets.
;  The symmetric difference is the set of items belonging to one but not both of the two sets.
(defn symdiff [s1 s2]
  (clojure.set/union (clojure.set/difference s1 s2)
                     (clojure.set/difference s2 s1)))

; alternative
(defn symdiff [s1 s2]
  (set (concat (remove s1 s2)
               (remove s2 s1))))

(filter even? (range 20))

(= (symdiff #{1 2 3 4 5 6} #{1 3 5 7}) #{2 4 6 7})
(= (symdiff #{:a :b :c} #{}) #{:a :b :c})
(= (symdiff #{} #{4 5 6}) #{4 5 6})
(= (symdiff #{[1 2] [2 3]} #{[2 3] [3 4]}) #{[1 2] [3 4]})


;; rotate sequence
;; Write a function which can rotate a sequence in either direction.

(defn rotate [x coll]
  (let [len (count coll)
        dist (mod x len)]
    (concat (drop dist coll)
            (take dist coll))))

(rotate 2 [1 2 3 4 5])
(rotate -2 [1 2 3 4 5])

(mod -12 5 )
(mod -2 5 )
          
(= (rotate 2 [1 2 3 4 5]) '(3 4 5 1 2))
(= (rotate -2 [1 2 3 4 5]) '(4 5 1 2 3))
(= (rotate 6 [1 2 3 4 5]) '(2 3 4 5 1))
(= (rotate 1 '(:a :b :c)) '(:b :c :a))
(= (rotate -4 '(:a :b :c)) '(:c :a :b))

(take 10 (cycle [:a :b :c]))


; reverse interleave
; Write a function which reverses the interleave process into x number of subsequences.

(defn separate [coll div]
  (vals (group-by (fn [x] (mod x div)) coll)))

; alternative solution
(defn separate [coll div]
  (apply map list (partition div coll)))

(def v [1 2 3 4 5 6])
(partition 2 v)                                             ; =>     ((1 2) (3 4) (5 6))
(map list '(1 2) '(3 4) '(5 6))                             ;=>      ((1 3 5) (2 4 6))
(apply map list '((1 2) (3 4) (5 6)))                       ;=>      ((1 3 5) (2 4 6))
(apply map list (partition 2 v))

; my solution is clearer in this case
; the alternative solution is more general, 
; for example, it could be used on non-number
; sequences

; however, my solution would be better on non-equal
; partitions, based on the function passed to group-by


(separate [1 2 3 4 5 6] 2) 

(= (separate [1 2 3 4 5 6] 2) '((1 3 5) (2 4 6)))
(= (separate (range 9) 3) '((0 3 6) (1 4 7) (2 5 8)))
(= (separate (range 10) 5) '((0 5) (1 6) (2 7) (3 8) (4 9)))

; split by type

; Write a function which takes a sequence consisting of items with different types and splits them up into a set of homogeneous sub-sequences. The internal order of each sub-sequence should be maintained, but the sub-sequences themselves can be returned in any order (this is why 'set' is used in the test cases).

(defn split-by-type [coll]
  (set (vals (group-by type coll))))

(split-by-type [1 :a 2 :b 3 :c])

(= (set (split-by-type [1 :a 2 :b 3 :c])) #{[1 2 3] [:a :b :c]})
(= (set (split-by-type [:a "foo"  "bar" :b])) #{[:a :b] ["foo" "bar"]})
(= (set (split-by-type [[1 2] :a [3 4] 5 6 :b])) #{[[1 2] [3 4]] [:a :b] [5 6]})

; count occurrences

; Write a function which returns a map containing the number of occurences of each distinct item in a sequence.

(defn my-frequencies [coll]
  (into {} (for [[k v] (group-by identity coll)]
             [k (count v)])))

(into {} (for [[k v] (group-by identity [1 1 2 3 2 1 1])]
           [k (count v)]))

(my-frequencies [1 1 2 3 2 1 1])

(my-frequencies [1 1 2 3 2 1 1])
(= (my-frequencies [1 1 2 3 2 1 1]) {1 4, 2 2, 3 1})
(= (my-frequencies [:b :a :b :a :b]) {:a 2, :b 3})
(= (my-frequencies '([1 2] [1 3] [1 3])) {[1 2] 1, [1 3] 2})


; read a binary number
; Convert a binary number, provided in the form of a string, to its numerical value.

(defn bin->dec [b]
  (let [num (if (= b "")
              0
              (Long/parseLong b))]
    (if (= num 0)
      0
      (+ (mod num 10)
         (* 2 (bin->dec (subs b 0 (dec (.length b)))))))))

;; alternative solution using java method to fullest:
#(Integer/parseInt % 2)

;;(subs "sarah" 0 -1)
;;(.length "sarah")

(bin->dec "11")

(bin->dec "1000")
(bin->dec "11111111")
(bin->dec "10101010101")
;;(first 234)
(first (str 234))
(Integer/parseInt (str (first (str 234))))

(= 0     (bin->dec "0"))
(= 7     (bin->dec "111"))
(= 8     (bin->dec "1000"))
(= 9     (bin->dec "1001"))
(= 255   (bin->dec "11111111"))
(= 1365  (bin->dec "10101010101"))
(= 65535 (bin->dec "1111111111111111"))

; dot product
; Create a function that computes the dot product of two sequences. You may assume that the vectors will have the same length.

(defn dot-product [v1 v2]
  (reduce + (map * v1 v2)))

(dot-product [0 1 0] [1 0 0])

(= 0 (dot-product [0 1 0] [1 0 0]))
(= 3 (dot-product [1 1 1] [1 1 1]))
(= 32 (dot-product [1 2 3] [4 5 6]))
(= 256 (dot-product [2 5 6] [100 10 1]))


; find distinct items
; Write a function which removes the duplicates from a sequence. Order of the items must be maintained.

#_(defn my-distinct [coll]
  (let [
        f-map (frequencies coll)
        elems (for [[k v] f-map
                    :when (> v 1)]
                k)]
    (remove elems coll)))


(defn my-distinct-helper [coll done]
  (let  [
          head (first coll)
          tail (rest coll)
          ]
      (cond
        (empty? coll) (list) 
        (done head) (my-distinct-helper tail done)
        :else (conj (my-distinct-helper 
                        tail
                        (conj done head))
                     head)))
  )

(defn my-distinct [coll]
  (my-distinct-helper coll #{}))

(def my-distinct (fn [coll]
                   (letfn [(helper [icoll done]
                             (let [head (first icoll)
                                   tail (rest icoll)]
                               (cond
                                 (empty? icoll) (list)
                                 (done head) (helper tail done)
                                 :else (conj (helper tail (conj done head))
                                             head))))]
                     (helper coll #{}))))


;; alternative solution

(fn [s] (reduce #(if (some #{%2} %1) %1 (conj %1 %2)) [] s))

(def my-distinct (fn [s] (reduce (fn [x y] (if (some #{y} x)
                                             x
                                             (conj x y)))
                                 []
                                 s)))


(distinct [1 2 1 3 1 2 4])

(my-distinct [1 2 1 3 1 2 4])
(my-distinct [:a :a :b :b :c :c])
(my-distinct (range 50))
(my-distinct '([2 4] [1 2] [1 3] [1 3]))


(= (my-distinct [1 2 1 3 1 2 4]) [1 2 3 4])
(= (my-distinct [:a :a :b :b :c :c]) [:a :b :c])
(= (my-distinct '([2 4] [1 2] [1 3] [1 3])) '([2 4] [1 2] [1 3]))
(= (my-distinct (range 50)) (range 50))

(concat 2 [])
(conj  #{1 2 3} 4)


#_(defn my-distinct [coll]
  (keys (frequencies coll)))

#_(defn my-distinct [coll]
  (let [s (set coll)])
  (for [x coll]
    :when (s x)))


(def done #{1 2 3})

(done 1)
(done 4)



#_(union #{1 2 3} #{4})

(clojure.set/union #{1 2 3} #{4})
#_(clojure.set/union #{1 2 3} 4)

(def coll [1 2 1 3 1 2 4])
(frequencies coll)


(concat [:foo] '())
(concat [:foo] nil )
coll
(frequencies coll)
(def f-map (frequencies coll))
f-map
(for [[k v] f-map
                    :when (> v 1)]
                k)




(def foo (fn [x] (* 2 x)))
foo

(apply + (filter even? (range 20)))

(macroexpand '(->> (range 20)
                  (filter even?)
                  (apply +)))

(* 2 3 4)



(defmacro infix
  "Use this macro when you pine for the notation of your childhood"
  [infixed]
  (list (second infixed) (first infixed) (last infixed)))

(infix (1 +  3))
(macroexpand '(infix (1 +  3)))

(map first (iterate (fn [[a b]] [b (+ a b)]) [0 1]))


(nth (map first (iterate (fn [[a b]] [b (+ a b)]) [0 1]))
     20)

(take 20 (iterate (fn [x] (+ x 3)) 1))
(fn [x] (* x 2))
#(* %1 %2 2)


; function composition
;; Write a function which allows you to create function compositions. The parameter list should take a variable number of functions, and create a function that applies them from right-to-left.


(defn my-comp [& args]
  (cond 
    (not (seq? (butlast args))) (last args)
    :else ((last args) (apply my-comp (butlast args)))))


(defn my-comp [& fs]
  (let [right-f (last fs)
        left-seq-f (butlast fs)]
    (fn [& args]
      (cond
        (empty? fs) (throw (IllegalArgumentException. "No function argument given to my-comp"))
        (empty? left-seq-f) (apply right-f args)
        :else ((apply my-comp left-seq-f) (apply right-f args))
        ))))

(defn my-comp [& fs]
  (let [right-f (last fs)
        left-fs (butlast fs)]
    (fn [& args]
      (if (empty? left-fs) 
        (apply right-f args)
        ((apply my-comp left-fs) (apply right-f args))))))


;; fn version for 4-clojure (doesn't allow defn)
(fn my-comp [& fs]
  (let [right-f (last fs)
        left-fs (butlast fs)]
    (fn [& args]
      (if (empty? left-fs)
        (apply right-f args)
        ((apply my-comp left-fs) (apply right-f args))))))


(= [3 2 1] ((my-comp rest reverse) [1 2 3 4]))
(= 5 ((my-comp (partial + 3) second) [1 2 3 4]))
(= true ((my-comp zero? #(mod % 8) +) 3 5 7 9))
(= "HELLO" ((my-comp #(.toUpperCase %) #(apply str %) take) 5 "hello world"))

#_(defn my-comp [& args]
  (fn [& act-args]
    (apply (last args) act-args)))

#_(defn my-comp [& f-list]
  (fn [& act-args]
    (reduce
      (fn [x f]
        (f x)
        )
      act-args
      (reverse f-list))))


;; only the first function can take multiple arguments
;; every function after that in the pipeline must take one
;; argument. So pass the actual-argument using apply only
;; in the base case 

((my-comp rest reverse) [1 2 3 4 5])

((my-comp rest reverse) [1 2 3 4])
((my-comp (partial + 3) second) [1 2 3 4])



(reduce + 10 (range 10))


(defrecord Person [name gender date-of-birth])

(let [x java.lang.Class]
  (and (= (class x)
          x)
       x))
(class =)
(class type)


;infix calculator

(defn positions [e s]
  (map first
       (filter #(= (second e))
               (map-indexed vector s))))

(defn infix [& args]
  (let [head (first args)
        tail (rest args)
        tokens (apply hash-map tail)]
    tokens))

(def s [20 / 2 + 2 + 4 + 8 - 6 - 10 * 9])
(filter #(and (number? (first %))
              (number? (last %))) 
        (partition 3 1 s))

s
(partition 2 (rest s))


(defn infix [& args]
  (reduce (fn [n1 [op n2]]
            (op n1 n2))
          (first args)
          (partition 2 (rest args))))

; http://commandercoriander.net/blog/2014/03/16/writing-an-infix-calculator-in-clojure/
(defn infix [val & others]
  (loop [acc   val
         stack others]
    (if (empty? stack)
      acc
      (let [op  (first stack)
            b   (second stack)
            rem (rest (rest stack))]
        (recur (op acc b) rem)))))

(apply infix s)
; but seems to give wrong answer, + 72 instead of - 72

(defn infix
  ([x op y]
   (op x y))
  ([x op y & xs]
   (apply infix (cons (infix x op y) xs))))

(apply infix s)


(->> (partition 3 1 s)
     (map (fn [x] (let [n1 (first x)
                        op (second x)
                        n2 (last x)] (if (or (= / op)
                                             (= * op))
                                       (list 0 + (op n1 n2))
                                       x)))

          )
     (filter #(and (number? (first %))
              (number? (last %))))
     #_(map #((second %) (first %)) )
     
     )
+
-
*
/

#_((10 + 0)
  (2 + 2)
  (2 + 4)
  (4 + 8)
  (8 - 6)
  (6 - 10)
  (90 * 0))


(def a [ 8 / 4])
(apply hash-map (rest a))

#_(defn infix [& args]
  (reduce #((first %2)
            )
          
          (first args)
          (partition 2 (rest args))))


;; "Assume a simple calculator that does not do precedence and instead just calculates left to right." 

(defn infix [& args]
  (reduce
    (fn [n1 [op n2]]
      (op n1 n2)) 
    (first args) 
    (partition 2 (rest args))))

(infix 20 / 2 + 2 + 4 + 8 - 6 - 10 * 9)

(infix 2 + 5)
(infix 38 + 48 - 2 / 2)
(infix 10 / 2 - 1 * 2)
(infix 20 / 2 + 2 + 4 + 8 - 6 - 10 * 9)

(= 7 (infix 2 + 5))
(= 42 (infix 38 + 48 - 2 / 2))
(= 8 (infix 10 / 2 - 1 * 2))
(= 72 (infix 20 / 2 + 2 + 4 + 8 - 6 - 10 * 9))

;; indexing sequences

;; Transform a sequence into a sequence of pairs containing the original elements along with their index.

(defn make-index [s]
  (map vector s (range (count s))))

(make-index [:a :b :c])

(= (make-index [:a :b :c]) [[:a 0] [:b 1] [:c 2]])
(= (make-index [0 1 3]) '((0 0) (1 1) (3 2)))
(= (make-index [[:foo] {:bar :baz}]) [[[:foo] 0] [{:bar :baz} 1]])


;; pascal's triangle
(defn p-triangle [n]
  (if (= n 1)
    [1]
    (concat [1] (map #(apply + %) (partition 2 1 (p-triangle (dec n)))) [1])))

(range 10)
(partition 2 1 [1 3 3 1])

(= (p-triangle 1) [1])
(p-triangle 1)
(p-triangle 2)
(p-triangle 3)

(= (map p-triangle (range 1 6))
   [     [1]
    [1 1]
    [1 2 1]
    [1 3 3 1]
    [1 4 6 4 1]])

(= (p-triangle 11)
   [1 10 45 120 210 252 210 120 45 10 1])



;  Given a function f and an input sequence s, return a lazy sequence of (f x) for each element x in s.

(defn my-map [f s]
  (if (empty? (rest s))
    (list (f (first s)))
    (lazy-seq (cons (f (first s))
                    (my-map f (rest s))))))

(my-map inc [2 3 4 5 6])
#_(sequential? [2 3 4 5 6])
#_(sequential? [])

#_(sequential? 2)                                           ; cannot be used to check if empty  
#_(sequential? [2])                                         ; neither can seq? (seq? vector) -> false
#_(sequential? [])
#_(first [])

(= [3 4 5 6 7]
   (my-map inc [2 3 4 5 6]))
(= (repeat 10 nil)
   (my-map (fn [_] nil) (range 10)))
(= [1000000 1000001]
   (->> (my-map inc (range))
        (drop (dec 1000000))
        (take 2)))

; partition a sequence
; Write a function which returns a sequence of lists of x items each. Lists of less than x items should not be returned.


(defn my-partition [n s]
  (if (< (count s) n)
    (list)
    (cons (take n s) (my-partition n (drop n s)))))

(take 3 [1 2])
(partition 2 [1 2])
(my-partition 3 (range 9))

(= (my-partition 3 (range 9)) '((0 1 2) (3 4 5) (6 7 8)))
(= (my-partition 2 (range 8)) '((0 1) (2 3) (4 5) (6 7)))
(= (my-partition 3 (range 8)) '((0 1 2) (3 4 5)))


; juxtaposition

; Take a set of functions and return a new function that takes a variable number of arguments and returns a sequence containing the result of applying each function left-to-right to the argument list.


(defn my-juxt [& args]
  (fn [& act-args] 
    (map 
      (fn [f] (apply f act-args))
      args)))

;; alternative solution from caterpillar

(fn myJuxt [& fs]
  (fn [& args]
    (for [f fs]
      (apply f args))))


(= [21 6 1] ((my-juxt + max min) 2 3 5 1 6 4))
(= ["HELLO" 5] ((my-juxt #(.toUpperCase %) count) "hello"))
(= [2 6 4] ((my-juxt :a :c :b) {:a 2, :b 4, :c 6, :d 8 :e 10}))


;; To Tree, or not to Tree

;; Write a predicate which checks whether or not a given sequence represents a binary tree. Each node in the tree must have a value, a left child, and a right child.

(defn tree? [t]
  (and 
    (coll? t)
    (= 3 (count t))
       (first t)
       (or (nil? (second t))
           (tree? (second t)))
       (or (nil? (last t))
           (tree? (last t)))))


(tree? '(:a (:b nil nil) nil))


(= (tree? '(:a (:b nil nil) nil))
   true)

(= (tree? '(:a (:b nil nil)))
   false)

(= (tree? [1 nil [2 [3 nil nil] [4 nil nil]]])
   true)

(= (tree? [1 [2 nil nil] [3 nil nil] [4 nil nil]])
   false)

(= (tree? [1 [2 [3 [4 nil nil] nil] nil] nil])
   true)

(= (tree? [1 [2 [3 [4 false nil] nil] nil] nil])
   false)

(= (tree? '(:a nil ()))
   false)

;; Sum of square of digits

;; 
;; Write a function which takes a collection of integers as an argument. Return the count of how many elements are smaller than the sum of their squared component digits. For example: 10 is larger than 1 squared plus 0 squared; whereas 15 is smaller than 1 squared plus 5 squared.

(defn sumsq [coll]
  )

(apply + (map #(* % %) (map #(Integer/parseInt %) (map #(str %) (seq (str 12345))))))

(->> 12345
     (str)
     (seq)
     (map #(str %))
     (map #(Integer/parseInt %))
     (map #(* % %))
     (apply + ))

((fn [n]
   ( <
     n
     (->> n
         (str)
         (seq)
         (map #(str %))
         (map #(Integer/parseInt %))
         (map #(* % %))
         (apply +))))
  12345)


(defn f [coll] (count (filter (fn [n]
                                (< n (->> n
                                          (str)
                                          (seq)
                                          (map #(str %))
                                          (map #(Integer/parseInt %))
                                          (map #(* % %))
                                          (apply +))))
                              coll )))

(range 10)
(map str (range 20))
(map seq (map str (range 20)))

(map #(Integer/parseInt %) (map str (range 20)))

(= 8 (f (range 10)))
(= 19 (f (range 30)))
(= 50 (f (range 100)))
(= 50 (f (range 1000)))


;; Word sorting
;; Write a function that splits a sentence up into a sorted list of words. Capitalization should not affect sort order and punctuation should be ignored.


(defn split-sort [a-str]
  (sort-by #(.toLowerCase %)
    (clojure.string/split a-str #"[\\ !,'\".]")))

;; alternative version with shorter reg-ex
(defn split-sort [a-str]
  (sort-by #(.toLowerCase %)
           (clojure.string/split a-str #"\W")))


(split-sort  "Have a nice day.")
(split-sort  "Clojure is a fun language!")
(split-sort  "Fools fall for foolish follies.")

String/CASE_INSENSITIVE_ORDER

(= (split-sort  "Have a nice day.")
   ["a" "day" "Have" "nice"])
(= (split-sort  "Clojure is a fun language!")
   ["a" "Clojure" "fun" "is" "language"])
(= (split-sort  "Fools fall for foolish follies.")
   ["fall" "follies" "foolish" "Fools" "for"])


;; Prime Numbers

;; Write a function which returns the first x number of prime numbers.

(defn primes [n]
  (drop 2 
        (take (+ 2 n) 
              (filter 
                (fn [x] (not-any? true? 
                                  (map #(= 0 (mod x %)) (range 2 x))))
                (range)))))

(def prime? (fn [x]
             (not-any? true? (map #(= 0 (mod x %)) (range 2 x)))))



(prime? 1)
(prime? 2)
(prime? 3)
(prime? 4)
(prime? 5)
(prime? 6)
(prime? 77)
(prime? 78)
(prime? 79)

(filter prime? (range 100))
(filter prime? (range 100000))


(primes 2)
(primes 5)
(primes 100)

(= (primes 2) [2 3])
(= (primes 5) [2 3 5 7 11])
(= (last (primes 100)) 541)

;; Recognize Playing Cards

;; A standard American deck of playing cards has four suits - spades, hearts, diamonds, and clubs - and thirteen cards in each suit. Two is the lowest rank, followed by other integers up to ten; then the jack, queen, king, and ace.

;;It's convenient for humans to represent these cards as suit/rank pairs, such as H5 or DQ: the heart five and diamond queen respectively. But these forms are not convenient for programmers, so to write a card game you need some way to parse an input string into meaningful components. For purposes of determining rank, we will define the cards to be valued from 0 (the two) to 12 (the ace)

;;Write a function which converts (for example) the string "SJ" into a map of {:suit :spade, :rank 9}. A ten will always be represented with the single character "T", rather than the two characters "10".

(defn parse-card [card]
  (let [
        suit (first card)
        rank (last card)
        suit-map {     \S :spade
                       \D :diamond
                       \C :club
                       \H :heart} 
        rank-map {
          \2 0 \3 1 \4 2 \5 3  \6 4  \7 5  \8 6 
          \9 7 \T 8 \J 9 \Q 10 \K 11 \A 12
         }
        ]
        {:suit (get suit-map suit)
         :rank (get rank-map rank)} ))


;; mfikes solution
(fn [c] {:suit ({\H :heart
                 \C :club
                 \S :spade
                 \D :diamond} (first c))
         :rank ((zipmap "23456789TJQKA" (range)) (second c))})
;; generates and uses the map right in the literal



(first "SJ")
(last "SJ")
(last "S6")
(parse-card "DQ")
(parse-card "H5")
(parse-card "CA")
(map (comp :rank parse-card str)
     '[S2 S3 S4 S5 S6 S7
       S8 S9 ST SJ SQ SK SA])

(= {:suit :diamond :rank 10} (parse-card "DQ"))
(= {:suit :heart :rank 3} (parse-card "H5"))
(= {:suit :club :rank 12} (parse-card "CA"))
(= (range 13) (map (comp :rank parse-card str)
                  '[S2 S3 S4 S5 S6 S7
                    S8 S9 ST SJ SQ SK SA]))


;; Longest Increasing Sub-Seq

;; Given a vector of integers, find the longest consecutive sub-sequence of increasing numbers. If two sub-sequences have the same length, use the one that occurs first. An increasing sub-sequence must have a length of 2 or greater to qualify.

(defn lss [v]
  (let [
        pairs (partition 2 1 v)
        partioned-by-LT (partition-by (fn [[a b]] (< a b)) pairs)
        increasing-partitions (filter (fn [x] (< (ffirst x) (second (first x)))) partioned-by-LT)
        
        longest-increasing-partition (reduce (fn [a b] (if (> (count b) 
                                                              (count a))
                                                            b
                                                            a)) 
                                              []
                                              increasing-partitions )
        head (ffirst longest-increasing-partition)
        tail (map last longest-increasing-partition)
        ]
    (if (nil? head)
      []
      (cons head tail))))

(lss v)

;; mfikes solution
(fn longest [coll]
  (let [candidate (apply max-key count
                         (reductions (fn [acc x]
                                       (if (empty? acc)
                                         [x]
                                         (if (= (inc (peek acc)) x)
                                           (conj acc x)
                                           [x])))
                                     []
                                     coll))]
    (if (> (count candidate) 1)
      candidate
      [])))

(def v [1 0 1 2 3 0 4 5])
(def w [5 6 1 3 2 7])
(def x [2 3 3 4 5])
(def y [7 6 5 4])

(lss y)

(partition-by (fn [[a b]] (< a b)) (partition 2 1 v))



(partition-by #(> % 0) (map (fn [[a b]] (- b a)) (partition 2 1 v)))
(partition-by #(> % 0) (map (fn [[a b]] (- b a)) (partition 2 1 w)))
(partition-by #(> % 0) (map (fn [[a b]] (- b a)) (partition 2 1 x)))
(partition-by #(> % 0) (map (fn [[a b]] (- b a)) (partition 2 1 y)))


(= (lss [1 0 1 2 3 0 4 5]) [0 1 2 3])
(= (lss [5 6 1 3 2 7]) [5 6])
(= (lss [2 3 3 4 5]) [3 4 5])
(= (lss [7 6 5 4]) [])

(reductions + (range 10))


v
(def v (range 10))
(def v [1 0 1 2 3 0 4 5])
(reductions (fn [coll x]
         (cond
           (empty? coll) [x]
           (< (last coll) x) (conj coll x)
           :else [x]))
       []
         v)
(apply max-key count (reductions
                       (fn [coll x]
                         (cond
                           (empty? coll) [x]
                           (< (last coll) x) (conj coll x)
                           :else [x]))
                       []
                       v))
(apply max-key count ["This" "is" "a" "very" "stooopid" "sentence"])


;; Black Box Testing
;; Clojure has many sequence types, which act in subtly different ways. The core functions typically convert them into a uniform "sequence" type and work with them that way, but it can be important to understand the behavioral and performance differences so that you know which kind is appropriate for your application.

;; Write a function which takes a collection and returns one of :map, :set, :list, or :vector - describing the type of collection it was given.
;; You won't be allowed to inspect their class or use the built-in predicates like list? - the point is to poke at them and understand their behavior.

(defn coll-type [coll]
  (let [test-coll (conj coll [:x :y])] (cond
                                  (= (get (conj test-coll [:c 3]) :c) 3) :map
                                  (= (last (into test-coll (range 50 100))) 99) :vector
                                  (= (first (conj test-coll :c)) :c) :list 
                                  (contains? (conj test-coll :c) :c) :set
                                   )))

(coll-type {:a 1, :b 2})
(coll-type (range (rand-int 20)))
(coll-type [1 2 3 4 5 6])
(coll-type #{10 (rand-int 5)})
(map coll-type [{} #{} [] ()])


(into [1 2 3] (range 50))

(keys [:c])

(concat '() [1])
(coll-type '(1))
(conj {:a 1 :b 2} [:x 3])
(conj #{} [:x :y])

(first {:a 1 :b 2})
(count {:a 1 :b 2})
(conj [:a :b] :c)
(conj '(:a :b) :c)
(conj #{:a :b} :c)
(contains?  (conj #{10 (rand-int 5)} :c) :c)
(first (conj (concat coll [1]) :c)) 
;; ('() :c)
;; (contains? (:a :b :c) :c)

(= :map (coll-type {:a 1, :b 2}))
(= :list (coll-type (range (rand-int 20))))
(= :vector (coll-type [1 2 3 4 5 6]))
(= :set (coll-type #{10 (rand-int 5)}))
(= [:map :set :vector :list] (map coll-type [{} #{} [] ()]))

(get (conj {:a 1 :b 2} [:c 3]) :c)
(get (conj [:a :b] [:c 3]) :c)
(get (conj [:a :b] [:c 3]) :c)

(/ (Math/log 7001)
   (Math/log 2))

(def ds (into-array [:willie :barnabas :adam]))
ds
(seq ds)
(sequential? ds)
(sequential? [])
(first [])
(rest [])
(seq [])
(seq [1])
(def egmap {:a 1 :b 2 :c 3})
(def egvec [:a :b :c])
(keys egmap)
(= (egmap (ffirst egmap)) (second (first egmap)))

(rest egmap)

(first {:a 1 :b 2})
(rest {:a 1 :b 2})

(aset ds 1 :quentin)

;; least common multiple

;; Write a function which calculates the least common multiple. Your function should accept a variable number of positive integers or ratios. 

(defn lcm
  ([x] x)
  ([x y]
   (second (filter #(= 0 (mod % y)) (iterate #(+ % x) 0))))
  ([x y & args]
    (reduce lcm (concat [x y] args))))

(lcm 2)
(lcm 2 3)
(lcm 2 3 5)

(concat [2 3] [4])

(== (lcm 2 3) 6)
(== (lcm 5 3 7) 105)
(== (lcm 1/3 2/5) 2)
(== (lcm 3/4 1/6) 3/2)
(== (lcm 7 5/7 2 3/5) 210)


;; Filter Perfect Squares


;; Given a string of comma separated integers, write a function which returns a new comma separated string that only contains the numbers which are perfect squares.

(defn psquares [coll-s]
  (->> (clojure.string/split coll-s #",")
       (map #(Integer/parseInt %))
       (filter #(= 0.0 (- (int (Math/sqrt %)) (Math/sqrt %))))
       (clojure.string/join ",")))



(= (psquares "4,5,6,7,8,9") "4,9")
(= (psquares "15,16,25,36,37") "16,25,36")

(= 3
   (let [[f a] [+ (range 3)]] (apply f a))
   (let [[[f a] b] [[+ 1] 2]] (f a b))
   (let [[f a] [inc 2]] (f a)))


;; The trampoline function takes a function f and a variable number of parameters. Trampoline calls f with any parameters that were supplied. If f returns a function, trampoline calls that function with no arguments. This is repeated, until the return value is not a function, and then trampoline returns that non-function value. This is useful for implementing mutually recursive algorithms in a way that won't consume the stack.


(= [1 3 5 7 9 11] 
   (letfn
     [(foo [x y] #(bar (conj x y) y))
      (bar [x y] (if (> (last x) 10)
                   x
                   #(foo x (+ 2 y))))]
     (trampoline foo [] 1)))


;; anagram finder

;; Write a function which finds all the anagrams in a vector of words. A word x is an anagram of word y if all the letters in x can be rearranged in a different order to form y. Your function should return a set of sets, where each sub-set is a group of words which are anagrams of each other. Each sub-set should have at least two words. Words without any anagrams should not be included in the result.

(defn anagrams [v]
  (->> v 
       (group-by frequencies)
       vals
       (filter #(> (count %) 1))
       (map set)
       set))


;; rob at funding circle's solution
(def v ["meat" "mat" "team" "mate" "eat"])
(partition-by #(apply str (sort %)) (sort-by #(apply str (sort %)) v))
(set (map set (vals (group-by sort v))))




(anagrams v)
(= (anagrams ["meat" "mat" "team" "mate" "eat"])
   #{#{"meat" "team" "mate"}})
(= (anagrams ["veer" "lake" "item" "kale" "mite" "ever"])
   #{#{"veer" "ever"} #{"lake" "kale"} #{"mite" "item"}})

(sort (seq "meat"))
(filter #(> (count %) 1) (partition-by identity (sort (map (fn [coll] (apply str coll)) (map sort ["meat" "mat" "team" "mate" "eat"])))))
;; possible strategies:
;; check length, check letter-set

(partition-by (fn [[x y]] ) ["meat" "mat" "team" "mate" "eat"])
() (vals (group-by first (map (fn [s] [(frequencies s) s]) v)))
(set (map set (vals (group-by frequencies v))))


(frequencies "")


#_(defn anagrams? [x y]
  (= (sort x) (sort y)))

(defn anagrams? [x y]
    (= (frequencies x) (frequencies y)))

(= (frequencies "meat")
   (frequencies "team"))

(sort "aabbcc")
(apply str (sort "meat"))

(anagrams? "meat" "mate")
(anagrams? "meat" "team")
(anagrams? "meat" "tea")


;;Perfect Numbers

;; A number is "perfect" if the sum of its divisors equal the number itself. 6 is a perfect number because 1+2+3=6. Write a function which returns true for perfect numbers and false otherwise.
(defn perfect? [x]
  (contains? (set (reductions + (range 1 x))) x))


(contains? (set (reductions + (range 1 496 ))) 496)
(int (/ (Math/log 16) (Math/log 2)))
(int (/ (Math/log 496) (Math/log 2)))

(= (perfect? 6) true)
(= (perfect? 7) false)
(= (perfect? 496) true)
(= (perfect? 500) false)
(= (perfect? 8128) true)



;; random r/clojure question
;; write a function to separate a string into 2 separate strings, one made up of the letters at odd positions, and the other at even positions of the string

(def b "abcdefg")
(defn split-string [s]
  (let [ltrs (partition-all 2 s)]
    [(apply str (map first ltrs))
     (apply str (map #(if (= 2 (count %)) (last %) nil) ltrs))])
  )
(split-string b)
(partition-all 2 b )
(apply str (map first (partition-all 2 b)))



(def a [:a :b :c :d :e :f :g])
(keep-indexed #(if true %1 %2) a)
a

;Sequence Reductions

;; Write a function which behaves like reduce, but returns each intermediate value of the reduction. Your function must accept either two or three arguments, and the return sequence must be lazy.

(defn reds
  ([f coll]
   (cond
     (empty? coll) (list (f))
     (empty? (rest coll)) (list (f (first coll)))
     :else (let [result (lazy-seq (reds f (drop-last coll)))]
             (lazy-cat result
                     [(f (last coll)
                         (last result))]))))
 ([f val coll]
   (reds f (cons val coll))))



;; is currently working for finite sequences
;; not working for infinite sequences
;; need to learn more about lazy sequences
;; doesn't make sense to call 'last' on a sequence that may be infinite :)
;; TODO rewrite, to avoid calling last on the result sequence

#_(= (take 5 (reds + (range))) [0 1 3 6 10])
#_(= (reds conj [1] [2 3 4]) [[1] [1 2] [1 2 3] [1 2 3 4]])
#_(= (last (reds * 2 [3 4 5])) (reduce * 2 [3 4 5]) 120)

(def v [1 2 3 4 5])
(def s (list 1 2 3 4 5))

(reds + [])
(reductions + [])
(reds + [1])
(reductions + [1])
(reds + [1 2])
(reductions + [1 2])
(reds +  (range 10) )
(reductions +  (range 10) )
(reds * [])
(reds + 100  (range 10) )
(reductions + 100  (range 10) )
(take 5 (reds + (range)))



;; pascals trapezoid
;; Write a function that, for any given input vector of numbers, returns an infinite lazy sequence of vectors, where each next one is constructed from the previous following the rules used in Pascal's Triangle. For example, for [3 1 2], the next row is [3 4 3 2].

;; Beware of arithmetic overflow! In clojure (since version 1.3 in 2011), if you use an arithmetic operator like + and the result is too large to fit into a 64-bit integer, an exception is thrown. You can use +' to indicate that you would rather overflow into Clojure's slower, arbitrary-precision bigint.

(defn pascal-trapezoid [v]
  (cond
    (empty? v)    (pascal-trapezoid [1])
    (empty? (rest v)) (concat ) 
    
    ))


(= (second (pascal-trapezoid [2 3 2])) [2 5 5 2])
(= (take 5 (pascal-trapezoid [1])) [[1] [1 1] [1 2 1] [1 3 3 1] [1 4 6 4 1]])
(= (take 2 (pascal-trapezoid [3 1 2])) [[3 1 2] [3 4 3 2]])
(= (take 100 (pascal-trapezoid [2 4 2])) (rest (take 101 (pascal-trapezoid [2 2]))))




